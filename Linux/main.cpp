/*
  Crude animation
  - Not the easiest way of doing things! 
*/

//For exit()
#include <stdlib.h>

//For printf()
#include <stdio.h>

//For round()
#include <math.h>

#if defined(_WIN32) || defined(_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const float FRAME_RATE = 60.0f;

const int SDL_OK = 0;

int main( int argc, char* args[] )
{
    // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    SDL_Texture*     playerTexture = nullptr;


    // Player variables
    const int PLAYER_SPRITE_WIDTH = 64;
    const int PLAYER_SPRITE_HEIGHT = 64;
    SDL_Rect standRight; // standing sprite
    SDL_Rect walk1;
    SDL_Rect walk2;
    SDL_Rect walk3;
    SDL_Rect walk4;
    SDL_Rect walk5;
    SDL_Rect walk6;
    SDL_Rect walk7;
    SDL_Rect walk8;
    SDL_Rect walk9;
    SDL_Rect walk10;
    SDL_Rect walk11;
    SDL_Rect walk12;
    SDL_Rect walk13;
    SDL_Rect walk14;
    SDL_Rect walk15;
    SDL_Rect walk16;

    SDL_Rect targetRectangle;
    float playerSpeed = 5.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;
    int frameNo = 1; //walk right frame control
    
    // Window control 
    SDL_Event event;
    bool quit = false;

    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // Timing variables
	unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
	double timeDeltaInSeconds;
    double fixedDt = 1.0f/FRAME_RATE;


    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
      
    /**********************************
     *    Setup background sprite     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/background.png");

    if(temp == nullptr)
    {
        printf("Background image not found!");
    }    

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    backgroundTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'image' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;


    /**********************************
     *    Setup mech sprite     *
     * ********************************/

    // Load the sprite to our temp surface
    temp = IMG_Load("assets/images/walker1.png");

    if(temp == nullptr)
    {
        printf("Walker image not found!");
    }

    // Create a texture object from the loaded image
    // - we need the renderer we're going to use to draw this as well!
    // - this provides information about the target format to aid optimisation.
    playerTexture = SDL_CreateTextureFromSurface(gameRenderer, temp);

    // Clean-up - we're done with 'temp' now our texture has been created
    SDL_FreeSurface(temp);
    temp = nullptr;

    //Setup source and destination rects
    targetRectangle.x = 0;
    targetRectangle.y = 0;
    targetRectangle.w = PLAYER_SPRITE_WIDTH;
    targetRectangle.h = PLAYER_SPRITE_HEIGHT;

    // Standing frame
    standRight.x = 2*PLAYER_SPRITE_WIDTH; //3rd col. 
    standRight.y = 2*PLAYER_SPRITE_HEIGHT; //3rd row
    standRight.w = PLAYER_SPRITE_WIDTH;
    standRight.h = PLAYER_SPRITE_HEIGHT;

    // Walking frames (always 3rd row). 
    walk1.x = 0*PLAYER_SPRITE_WIDTH;
    walk1.y = 2*PLAYER_SPRITE_HEIGHT;
    walk1.w = PLAYER_SPRITE_WIDTH;
    walk1.h = PLAYER_SPRITE_HEIGHT;

    walk2.x = 1*PLAYER_SPRITE_WIDTH;
    walk2.y = 2*PLAYER_SPRITE_HEIGHT;
    walk2.w = PLAYER_SPRITE_WIDTH;
    walk2.h = PLAYER_SPRITE_HEIGHT;

    walk3.x = 2*PLAYER_SPRITE_WIDTH;
    walk3.y = 2*PLAYER_SPRITE_HEIGHT;
    walk3.w = PLAYER_SPRITE_WIDTH;
    walk3.h = PLAYER_SPRITE_HEIGHT;

    walk4.x = 3*PLAYER_SPRITE_WIDTH;
    walk4.y = 2*PLAYER_SPRITE_HEIGHT;
    walk4.w = PLAYER_SPRITE_WIDTH;
    walk4.h = PLAYER_SPRITE_HEIGHT;

    walk5.x = 4*PLAYER_SPRITE_WIDTH;
    walk5.y = 2*PLAYER_SPRITE_HEIGHT;
    walk5.w = PLAYER_SPRITE_WIDTH;
    walk5.h = PLAYER_SPRITE_HEIGHT;

    walk6.x = 5*PLAYER_SPRITE_WIDTH;
    walk6.y = 2*PLAYER_SPRITE_HEIGHT;
    walk6.w = PLAYER_SPRITE_WIDTH;
    walk6.h = PLAYER_SPRITE_HEIGHT;

    walk7.x = 6*PLAYER_SPRITE_WIDTH;
    walk7.y = 2*PLAYER_SPRITE_HEIGHT;
    walk7.w = PLAYER_SPRITE_WIDTH;
    walk7.h = PLAYER_SPRITE_HEIGHT;

    walk8.x = 6*PLAYER_SPRITE_WIDTH;
    walk8.y = 2*PLAYER_SPRITE_HEIGHT;
    walk8.w = PLAYER_SPRITE_WIDTH;
    walk8.h = PLAYER_SPRITE_HEIGHT;

    walk9.x = 8*PLAYER_SPRITE_WIDTH;
    walk9.y = 2*PLAYER_SPRITE_HEIGHT;
    walk9.w = PLAYER_SPRITE_WIDTH;
    walk9.h = PLAYER_SPRITE_HEIGHT;

    walk10.x = 9*PLAYER_SPRITE_WIDTH;
    walk10.y = 2*PLAYER_SPRITE_HEIGHT;
    walk10.w = PLAYER_SPRITE_WIDTH;
    walk10.h = PLAYER_SPRITE_HEIGHT;

    walk11.x = 10*PLAYER_SPRITE_WIDTH;
    walk11.y = 2*PLAYER_SPRITE_HEIGHT;
    walk11.w = PLAYER_SPRITE_WIDTH;
    walk11.h = PLAYER_SPRITE_HEIGHT;

    walk12.x = 11*PLAYER_SPRITE_WIDTH;
    walk12.y = 2*PLAYER_SPRITE_HEIGHT;
    walk12.w = PLAYER_SPRITE_WIDTH;
    walk12.h = PLAYER_SPRITE_HEIGHT;

    walk13.x = 12*PLAYER_SPRITE_WIDTH;
    walk13.y = 2*PLAYER_SPRITE_HEIGHT;
    walk13.w = PLAYER_SPRITE_WIDTH;
    walk13.h = PLAYER_SPRITE_HEIGHT;

    walk14.x = 13*PLAYER_SPRITE_WIDTH;
    walk14.y = 2*PLAYER_SPRITE_HEIGHT;
    walk14.w = PLAYER_SPRITE_WIDTH;
    walk14.h = PLAYER_SPRITE_HEIGHT;

    walk15.x = 14*PLAYER_SPRITE_WIDTH;
    walk15.y = 2*PLAYER_SPRITE_HEIGHT;
    walk15.w = PLAYER_SPRITE_WIDTH;
    walk15.h = PLAYER_SPRITE_HEIGHT;

    walk16.x = 15*PLAYER_SPRITE_WIDTH;
    walk16.y = 2*PLAYER_SPRITE_HEIGHT;
    walk16.w = PLAYER_SPRITE_WIDTH;
    walk16.h = PLAYER_SPRITE_HEIGHT;

    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quite is not true
    { 

        // Calculate time elapsed
        // Better approaches to this exist - https://gafferongames.com/post/fix_your_timestep/
		currentTimeIndex = SDL_GetTicks();
		timeDelta = currentTimeIndex - prevTimeIndex;

		// Store current time index into prevTimeIndex for next frame
		prevTimeIndex = currentTimeIndex;

     	timeDeltaInSeconds = float(timeDelta) / 1000.0f;

        // Handle input 

        if( SDL_PollEvent( &event ))  // tes for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;

                    case SDLK_UP:
                        verticalInput = -1.0f;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    
                    case SDLK_UP:
                        verticalInput = 0.0f;
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        // Update Game World

        // Calculate player velocity. 
	// Note: This is imperfect, no account taken of diagonal!
        float yMovement = timeDeltaInSeconds * (verticalInput * playerSpeed);
        float xMovement = timeDeltaInSeconds * (horizontalInput * playerSpeed);
	
        // Update player position.
	playerX += xMovement;
        playerY += yMovement;

        // Move sprite to nearest pixel location.
	targetRectangle.y = round(playerY);
	targetRectangle.x = round(playerX);

        // Render (draw) Game World

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
       
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);
        
	switch(frameNo)
        {
             case 1:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk1, &targetRectangle);
             break;
             case 2:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk2, &targetRectangle);
             break;
             case 3:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk3, &targetRectangle);
             break;
             case 4:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk4, &targetRectangle);
             break;
             case 5:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk5, &targetRectangle);
             break;
             case 6:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk6, &targetRectangle);
             break;
             case 7:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk7, &targetRectangle);
             break;
             case 8:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk8, &targetRectangle);
             break;
             case 9:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk9, &targetRectangle);
             break;
             case 10:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk10, &targetRectangle);
             break;
             case 11:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk11, &targetRectangle);
             break;
             case 12:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk12, &targetRectangle);
             break;
             case 13:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk13, &targetRectangle);
             break;
             case 14:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk14, &targetRectangle);
             break;
             case 15:
                 SDL_RenderCopy(gameRenderer, playerTexture, &walk15, &targetRectangle);
             break;
        }
 
        //Advance to next frame (or return to start)
        if(frameNo < 15)
            frameNo++;
        else
            frameNo = 1;
     
        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);


        // Pause to fudge animation rate
        SDL_Delay(300);
    }

    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;   
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resorces etc.
    SDL_Quit();

    return 0;
}
